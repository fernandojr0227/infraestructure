resource "aws_ecr_repository" "ecr_registry" {
  name                 = var.ecr_name
  image_tag_mutability = var.image_tag_mutable

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }
}
