## Provisionamento Automático
GitLab pipeline

## Provisionamento Manual

```sh
terraform init
```

## Aplicando

```sh
terraform apply --auto-approve
```

## Validação

```sh
terraform validate
```

## Adicionando o contexto do cluster ao kubectl

```bash
aws eks --region sa-east-1 update-kubeconfig --name k8s-preprod-environment
```