provider "aws" {
  region = var.aws_region
}

# terraform {
#   backend "s3" {
#     bucket = "fernando.rodrigues-tf.state"
#     key    = "terraform.tfstate"
#     region = "sa-east-1"
#   }
# }